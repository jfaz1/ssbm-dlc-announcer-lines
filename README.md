# SSBM DLC Announcer Lines
Voice lines from the great Dean Harrington, the original Melee announcer, announcing characters that never made it into the game.

# Credits:
[Dean Harrington](https://www.deanharringtonvisual.com/voice_actor/) for being generous enough to donate these voice lines. Please support him if you have the chance!

[MelonSpeedRuns](https://www.youtube.com/c/MelonSpeedruns) and [David V. Kimball](https://davidvkimball.com/) for getting in contact with Dean Harrington and arranging for all these clips to be recorded.

[jfaz (Pepe)](https://discordapp.com/users/578762638213775370) for mixing raw audio (making it Melee) and creating this repo.

# Site
Site is located at: 

https://dean.melee.tv
